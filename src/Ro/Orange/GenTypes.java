package Ro.Orange;


//A public class with two parameters named T1 and T2
public class GenTypes<T1,T2> {

    //Two objects for T1 and T2 named : name and value
    private T1 name;
    private T2 value;

    //constructor
    GenTypes(T1 name, T2 value) {
        this.name = name;
        this.value = value;
    }

    //Getters methods for these two objects
    public T1 getName() {
        return name;
    }

    public T2 getValue() {
        return value;
    }
}
