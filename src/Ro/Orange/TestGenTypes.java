package Ro.Orange;

public class TestGenTypes {

    public static void main(String[] args) {
	//Create one instantiation for the GenTypes class
        GenTypes<String,Integer> employee = new GenTypes<>("David Gilbert",1700);

    //Print the employee’s name and his salary
        System.out.println("Employee: " + employee.getName() + "\nSalary: " + employee.getValue());


    }
}
